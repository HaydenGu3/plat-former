﻿using UnityEngine;
using System.Collections;

public class EasyMove : MonoBehaviour {

	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip coinSound;
	
	public float Speed = 0f;
	public float MaxJumpTime = 2f;
	public float JumpForce;
	private float move = 0f;
	private float JumpTime = 0f;
	private bool CanJump;
	
	
	void Start () {
		JumpTime  = MaxJumpTime;
	}
	
	
	void Update ()
	{
		if (!CanJump)
			JumpTime  -= Time.deltaTime;
		if (JumpTime <= 0)
		{
			CanJump = true;
			JumpTime  = MaxJumpTime;
		}
	}
	
	void FixedUpdate () {
		move = Input.GetAxis ("Horizontal");
		GetComponent<Rigidbody2D>().velocity = new Vector2 (move * Speed, GetComponent<Rigidbody2D>().velocity.y);
		if (Input.GetKey (KeyCode.W)  && CanJump)
		{
			GetComponent<Rigidbody2D>().AddForce (new Vector2 (GetComponent<Rigidbody2D>().velocity.x,JumpForce));
			CanJump = false;
			JumpTime  = MaxJumpTime;
		}
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollideWith){
			if (objectPlayerCollideWith.tag == "Coin") {
			//playerHealth += healthPerFruit;
			//GameController.Instance.healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollideWith.gameObject.SetActive (false);
			SoundController.Instance.PlaySingle (coinSound);
		}
	}

}