﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
	
	public static GameController Instance;
	
	private GameObject startImage;
	private bool settingUpGame;
	public Text healthText;

	
	void Awake () {
		if (Instance != null && Instance != this) {
			DestroyImmediate(gameObject);
			return;
		}
		Instance = this;
	}
	
	void Start(){
		InitializeGame ();
	}


	
	private void InitializeGame(){
		startImage = GameObject.Find ("Start Image");
		startImage.SetActive (true);
		settingUpGame = true;
	}

	public void CancelInvoke(){
		startImage = GameObject.Find ("Start Image");
		startImage.SetActive (false);
		settingUpGame = false;
	}
	

	
//	public void GameOver(){
//		isPlayerTurn = false;
//		SoundController.Instance.music.Stop();
//		SoundController.Instance.PlaySingle(gameOverSound);
		
//		if (currentLevel > 1) {
//			levelText.text = "You starved after " + currentLevel + " days...";
//		}
//		else {
//			levelText.text = "You starved after " + currentLevel + " day...";
//		}
		
//		levelImage.SetActive (true);
//		enabled = false;
//		
//	}
}